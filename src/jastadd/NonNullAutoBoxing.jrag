// Combine NonNull with Autoboxing from Java5
aspect NonNullAutoboxing {
	// The unboxed type of a TaggedClassDecl is the same as for its original class
	eq TaggedClassDecl.unboxed() = getClassDecl().unboxed();

	// With NonNull typing, the boxed type of a primitive is the corresponding NonNull variant of the normal boxed type.
	refine AutoBoxing eq BooleanType.boxed() = ((ClassDecl)lookupType("java.lang", "Boolean")).nonNullTypeDecl();
	refine AutoBoxing eq ByteType.boxed() = ((ClassDecl)lookupType("java.lang", "Byte")).nonNullTypeDecl();
	refine AutoBoxing eq CharType.boxed() = ((ClassDecl)lookupType("java.lang", "Character")).nonNullTypeDecl();
	refine AutoBoxing eq ShortType.boxed() = ((ClassDecl)lookupType("java.lang", "Short")).nonNullTypeDecl();
	refine AutoBoxing eq IntType.boxed() = ((ClassDecl)lookupType("java.lang", "Integer")).nonNullTypeDecl();
	refine AutoBoxing eq LongType.boxed() = ((ClassDecl)lookupType("java.lang", "Long")).nonNullTypeDecl();
	refine AutoBoxing eq FloatType.boxed() = ((ClassDecl)lookupType("java.lang", "Float")).nonNullTypeDecl();
	refine AutoBoxing eq DoubleType.boxed() = ((ClassDecl)lookupType("java.lang", "Double")).nonNullTypeDecl();
}
