package test;

public class Test08 {

    @NonNull Integer integerField01 = 0;
    @NonNull Integer integerField02 = new Integer(5);
             Integer integerField03 = 1;

    public void testAutoBoxing01() {
        this.integerField01 = 13;
    }

    public void testAutoBoxing02() {
        @NonNull Integer integerLocVar01 = 14;
        @NonNull Integer integerLocVar02 = new Integer(1);
		@NonNull Integer integerLocVar03 = 2*5;

        int intLocVar01 = integerLocVar02;
        integerLocVar01 = integerLocVar02;
    }

}
