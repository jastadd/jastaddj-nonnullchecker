JastAddJ-NonNullChecker
=======================

This module is an extension to the JastAdd extensible Java compiler (JastAddJ), adding non-null type analysis. The resulting tool, _NonNullChecker_, detects possible null-pointer violations at compile time.

Non-null types is a type-based approach to detect possible null pointer violations in code statically at compile time. Fähndrich and Leino showed how an object-oriented language such as Java or C# could be extended with non-null types. We have extended JastAddJ Java compiler, in a modular way, to include non-null types. This is an example of how a large JastAdd project can be extended in an easy and declarative way.

We have also implemented an inference algorithm to retrofit legacy code to include non-null types. See the companion module _JastAddJ-NonNullInference_.

License & Copyright
-------------------

 * Copyright (c) 2005-2014, JastAddJ-NonNullChecker Committers

All rights reserved.

JastAddJ-NonNullChecker is covered by the Modified BSD License. The full license text is distributed with this software. See the `LICENSE` file.

NonNullChecker
--------------

The non-null checker relies on type annotations to detect possible null-pointer violations. Consider the code snippet below:

    class C {
      void test() { }
        public static void main(String[] args) {
          @NonNull C c1 = new C(); // c1 can only hold non-null values
          C c2 = null; // c2 can hold possibly-null values
          c1.test();   // ok: c1 can not be null
          c2.test();   // error: c2 may be null and the invocation may 
                       //        throw a null pointer exception
          c1 = c2;     // error: a non-null variable may not be assigned a 
                       //        possibly null value
          c2 = c1;     // ok: a possibly-null variable may be assigned a 
                       //     non-null value
          if(c2 != null) {
            c1 = c2;   // ok: an explicit comparison to null acts as a
                       //     safe cast as long as there are no possibly
                       //     null assignments in the block
          }
       }
    }

If we run the above program through the non-null checker we get the following output:

    $ java -jar JavaNonNullChecker.jar C.java
    $ Errors:
    $ C.java:7:
    $ Semantic Error: qualifier c2 may be null
    $ C.java:9:
    $ Semantic Error: can not assign c1 of type C- a value of type C

The same program can be compiled using a normal Java compiler, but then the possible null-pointer violations would not be detected.

Further details
---------------

The following paper describes how JastAddJ is extended with NonNull type checking and inferencing:

 * __Pluggable checking and inferencing of non-null types for Java__  
   _Torbjörn Ekman, Görel Hedin_  
   Journal of Object Technology, Vol. 6, No. 9, pages 455-475.   
   Special Issue: TOOLS EUROPE 2007, October 2007.  
   [Download paper here](http://dx.doi.org/10.5381/jot.2007.6.9.a23)

The work by Fähndrich and Leino is available in:

 * __Declaring and Checking Non-Null Types in an Object-Oriented Language__  
   _Manuel Fähndrich, Rustan Leino_  
   Proceedings of the 18th ACM Conference on Object-Oriented Programming Systems, 
   Languages, and Applications (OOPSLA'03), Anaheim, CA, October 2003.  
   [Download paper here](http://doi.acm.org/10.1145/949305.949332)

Building
--------

This module should be placed as a sibling directory of the modules _JastAddJ_ and _JastAddJ-JSR308_ (available at [jastadd.org](http://jastadd.org)). You only need to have javac and Apache Ant installed in order to build. All other tools used are available in JastAddJ.

Build targets:

    $ ant         # Build the NonNullChecker tool as java class files
    $ ant jar     # Build the NonNullChecker tool as a jar file
    $ ant test    # Build the tool and run the test suite
    $ ant clean   # Remove generated files

Tests
-----

The main program `RunTests.java` is a test harness which collects and executes all test cases found in the `test` folder.
Each test is simply a `.java` file and a corresponding `.result` file holding the expected result.

Variants
--------

By default, the `build.xml` file builds the tool for Java 5 extended with JSR308. As an alternative, it is possible to build for plain Java 1.4 or Java 5 (without JSR308). This is done by supplying a property argument when building, as follows:

    $ ant -Djava4=true test
    $ ant -Djava5=true test

Note that if you check out older versions of the modules (2013 and previous), you need to rename the JastAddJ-JSR308 module to JSR308.